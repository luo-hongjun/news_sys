package controllers

import (
	"github.com/astaxie/beego/gomail.v2"
	//"github.com/beego/beego/v2/client/orm"
	"github.com/astaxie/beego/orm"
	"strconv"

	//"github.com/astaxie/beego/utils"
	//"github.com/beego/beego/v2/adapter/utils"
	beelog "github.com/beego/beego/v2/logs"
	beego "github.com/beego/beego/v2/server/web"
	"newspass/models"
	"time"
)

type RegController struct {
	beego.Controller
}
type LogController struct {
	beego.Controller
}

func (this *RegController) ShowReg() {
	this.TplName = "register.html"
}

func (this *RegController) AddReg() {
	//获取数
	name := this.GetString("userName")
	password := this.GetString("password")
	if name == "" || password == "" {
		beelog.Info("用户或者密码不能为空", name, password)
		this.TplName = "register.html"
		return
	}
	o := orm.NewOrm()
	user := models.User{}
	user.Username = name
	user.Password = password
	_, err := o.Insert(&user)
	if err != nil {
		beelog.Info("db inster err", err)
		this.TplName = "register.html"
		return
	}
	this.Redirect("/login", 302)
	//this.TplName="login.html"
}
func (this *LogController) LogShow() {
	name := this.Ctx.GetCookie("userName")

	if name != "" {
		this.Data["checked"] = "checked"
		this.Data["userName"] = name
	}
	this.TplName = "login.html"
}

func (this *LogController) Login() {
	name := this.GetString("userName")
	passwd := this.GetString("password")
	check := this.GetString("remember")
	beelog.Info(check)
	if name == "" || passwd == "" {
		beelog.Info("用户密码为空")
		return
	}
	o := orm.NewOrm()
	user := models.User{}
	user.Username = name
	err := o.Read(&user, "username")
	if err != nil {
		beelog.Info("用户名出错")
		return
	}
	if user.Password != passwd {
		beelog.Info("密码错误")
		return
	}
	if check != "" {
		this.Ctx.SetCookie("userName", name, time.Second*3600) //
	} else {
		this.Ctx.SetCookie("userName", name, -1) //设置有效时间为-1，相当于不保存cookie
	}

	this.SetSession("userName", name)

	this.Redirect("/article/showArticleList", 302)
}
func (this *LogController) Logout() {
	this.DelSession("userName")
	this.Redirect("/login", 302)
}
func (this *LogController) ActiveMail() {
	mailConn := map[string]string{
		"user": "405910024@qq.cn",
		"pass": "hlxjqdcgobejcbdf",
		"host": "smtp.exmail.qq.com",
		"port": "465",
	}

	port, _ := strconv.Atoi(mailConn["port"])

	m := gomail.NewMessage()

	m.SetHeader("From", m.FormatAddress(mailConn["user"], "测试邮件"))

	m.SetHeader("To", "405910024@qq.com") //发送给多个用户
	m.SetHeader("Subject", "设置邮件主题")      //设置邮件主题
	m.SetBody("text/html", "邮件正文")        //设置邮件正文

	d := gomail.NewDialer(mailConn["host"], port, mailConn["user"], mailConn["pass"])

	err := d.DialAndSend(m)
	if err != nil {
		beelog.Info("发送失败:", err)
	}

	//emailconfig := `{"username":"405910024@qq.com","password":"yjtbsmyfnnjybhaa","host":"smtp.qq.com","port":"587"}`
	//econn := utils.NewEMail(emailconfig)
	//econn.From = "天天生鲜官方邮件"
	////目标邮箱
	//econn.To = []string{"405910024@qq.com"}
	////标题
	//econn.Subject = "天天生鲜激活邮件"
	////内容
	//econn.Text = "激活地址！"
	//econn.Send()
}
